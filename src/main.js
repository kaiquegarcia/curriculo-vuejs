import Vue from 'vue'
import VuePwaInstallPlugin from "vue-pwa-install";
import App from './App.vue'
import store from "./store/index";
import router from "./router/index";
import "bootstrap/dist/css/bootstrap.min.css";
import "./registerServiceWorker";

Vue.config.productionTip = false
Vue.use(VuePwaInstallPlugin);

new Vue({
    render: h => h(App),
    store,
    router
}).$mount('#app')
