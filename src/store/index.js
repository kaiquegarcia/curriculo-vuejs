import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

import config from "./modules/config";
import author from "./modules/author";
import career from "./modules/career";
import hobbies from "./modules/hobbies";
import languages from "./modules/languages";
import skills from "./modules/skills";
import timeline from "./modules/timeline";


export default new Vuex.Store({
    modules: {
        author,
        config,
        career,
        hobbies,
        languages,
        skills,
        timeline
    }
});

