const state = {
    "i18n": {
        "pt-br": {
            "function": `Desenvolvedor Backend Sênior`
        },
        "en": {
            "function": "Senior Backend Developer"
        }
    },
    "name": "Kaique Garcia",
    "email": "contato@kaiquegarcia.dev",
    "telegram": "kg_thebest",
    "links": [
        {
            "url": "https://linkedin.com/in/kaiquegarcia",
            "icon": "fab fa-linkedin-in fa-fw",
            "text": "LinkedIn"
        },
        {
            "url": "https://github.com/kaiquegarcia",
            "icon": "fab fa-github-alt fa-fw",
            "text": "GitHub"
        },
        {
            "url": "https://bitbucket.org/kaiquegarcia/",
            "icon": "fab fa-bitbucket",
            "text": "Bitbucket"
        },
        {
            "url": "https://blog.kaiquegarcia.dev/",
            "icon": "fa fa-newspaper",
            "text": "Blog"
        }
    ]
};

export default {
    namespaced: true,
    state
}
