const state = {
    "i18n": {
        "pt-br": {
            "title": "Experiência de Trabalho",
            "goals": "Metas",
            "goal_dynamic": "Tornar nossa equipe de tecnologia em uma equipe dinâmica",
            "goal_sub_teams": "Sub-equipes por projeto",
            "goal_two_week_sprint": "Sprint de duas semanas",
            "goal_long_backlog": "Backlog com previsão mínima de três meses",
            "technologies": "Tecnologias",
            "developer": "Desenvolvedor Web Full Stack",
            "php_developer": "Desenvolvedor PHP Pleno",
            "staff_developer": "Desenvolvedor de Software Especialista",
            "sr_back_developer": "Desenvolvedor Backend Sênior",
            "software_engineer": "Engenheiro de Software",
            "coordinator": "Coordenador de Tecnologia",
            "present": "presente"
        },
        "en": {
            "title": "Work Experience",
            "goals": "Goals",
            "goal_dynamic": "Make our technology team a dynamic team",
            "goal_sub_teams": "Sub-teams per project",
            "goal_two_week_sprint": "Two-week sprint",
            "goal_long_backlog": "Backlog with a minimum forecast of three months",
            "technologies": "Technologies",
            "developer": "Full Stack Web Developer",
            "php_developer": "PHP Developer",
            "staff_developer": "Staff Software Developer",
            "sr_back_developer": "Senior Backend Developer",
            "software_engineer": "Software Engineer",
            "coordinator": "Technology Coordinator",
            "present": "present"
        }
    },
    "companies": [
        {
            "name": "Freedom Digital",
            "link": "https://www.freedomdigital.com.br"
        },
        {
            "name": "Explicaê",
            "link": "https://explicae.com.br"
        },
        {
            "name": "PicPay",
            "link": "https://picpay.com.br"
        },
        {
            "name": "Linx",
            "link": "https://linx.com.br"
        },
        {
            "name": "Kavak",
            "link": "https://kavak.com"
        }
    ],
    "events": [
        {
            "company": 4,
            "function": "sr_back_developer",
            "from": 2022,
            "to": null,
            "resume": {
                "pt-br": `<p>Comecei atuando no time de anti-fraude do México, mas, 6 meses depois, migrei para o time responsável por produzir e sustentar os sistemas por trás dos hubs de atendimento da empresa ao redor do globo. Minhas entregas faziam parte da experiência do usuário que visita nossos hubs para fazer quaisquer tipos de atendimento. Nós terminamos uma nova plataforma que reduziu os custos mensais da Kavak por cancelar os contratos com sistemas externos como a Monday. Agora esou trabalhando no time Martech, responsável por gerenciar e evoluir as tecnologias do time de marketing. Minha principal missão é a de manter os custos baixos com melhores integrações entre tudo que temos por aqui.</p>`,
                "en": `<p>I started working on the anti-fraud team in Mexico but 6 months later I've been moved to the team responsible for producing and sustaining the systems behind the company's hubs around the globe. My deliveries were part of the experience of the user who visits our hubs to request any kind of service. We've finished a new platform that reduced Kavak's monthly costs by cancelling the contract with external softwares like Monday. Now I'm working on the Martech team, responsible to manage and evolve the technology of the marketing team. My prior mission is to keep the costs low with better integrations with everything we have.</p>`
            },
            "goals": [],
            "technologies": [
                "Golang", "Node.js", "Typescript", "Angular", "SASS", "Apache Kafka"
            ]
        },
        {
            "company": 3,
            "function": "staff_developer",
            "from": 2021,
            "to": 2022,
            "resume": {
                "pt-br": `<p>Apesar do curto tempo, consegui corrigir alguns bugs que existiam há mais de 3 anos e mapeei mais de 50 (cinquenta) débitos técnicos. Acredito ter superado as expectativas sobre mim e eu estava cada vez mais envolvido nas necessidades das aplicações da Chaordic (contratante original).</p>`,
                "en": `<p>Despite the short time, I managed to fix some bugs that existed for over 3 years and mapped more than 50 (fifty) technical debts. I believe I exceeded expectations about me and I was increasingly involved in the needs of Chaordic's applications (original contractor).</p>`
            },
            "goals": [],
            "technologies": [
                "PHP", "Lumen", "Laravel"
            ]
        },
        {
            "company": 2,
            "function": "software_engineer",
            "from": 2020,
            "to": 2021,
            "resume": {
                "pt-br": `<p>Entrei no time de Assinaturas, revisando documentações, levantando débitos técnicos e exercendo algumas manutenções. Posteriormente, assumi o papel de Tech Leader e comecei a liderar várias investigações para o time de produto. Consegui ressaltar mudanças importantes no período da pandemia, onde consegui destacar a importância de recursos mais voltados ao mercado on-line do que o presencial, afinal, tudo estava fechado. Minha atuação resultou na criação de nova tribo e novos times. Além disso, atuei em novos experimentos na busca de recursos capazes de criar mais produtos para o super app da Picpay.</p>`,
                "en": `<p>I joined the Subscriptions team, reviewing documentation, raising technical debts and carrying out some maintenance. Later, I assumed the role of Tech Leader and started to lead several investigations for the product team. I managed to highlight important changes during the pandemic period, where I was able to highlight the importance of resources more focused on the online market than face-to-face. My performance resulted in the creation of a new tribe and new teams. In addition, I worked on new experiments in the search for resources capable of creating more products for the Picpay' super app.</p>`
            },
            "goals": [],
            "technologies": [
                "PHP",
                "JavaScript",
                "Laravel",
                "MySQL",
                "MongoDB",
                "Redis",
                "Apache Kafka",
                "Beanstalkd",
                "Harness",
                "Drone.io",
                "Sonar Cloud",
                "Angular"
            ]
        },
        {
            "company": 1,
            "function": "developer",
            "from": 2020,
            "to": 2020,
            "resume": {
                "pt-br": `<p>Com a chegada da nova chefia da empresa, retornei ao meu posicionamento inicial: desenvolvimento.</p><p>Trabalho na plataforma web no que for necessário, participando, também, no gerenciamento do código.</p>`,
                "en": `<p>With the arrival of the new head of the company, I returned to my initial position: development.</p><p>I work on the web platform as needed, also participating in code management.</p>`
            },
            "goals": [],
            "technologies": [
                "PHP",
                "JavaScript",
                "jQuery",
                "Laravel",
                "HTML5/SASS",
                "MySQL",
                "Vue.js",
                "Node.js"
            ]
        },
        {
            "company": 1,
            "function": "coordinator",
            "from": 2019,
            "to": 2020,
            "resume": {
                "pt-br": `<p>Com o crescimento da empresa, acabei assumindo essa posição para tentar gerenciar o setor de tecnologia.</p><p>Embora seja um grande desafio, estamos nos adaptando para obter o melhor desempenho de todos os membros da equipe.</p><p>Por exemplo, ao invés de gerenciar o scrum <small>(que era a ideia inicial)</small>, eu apenas auxilio nas tomadas de decisão. Na maior parte do tempo, continuo desenvolvendo projetos.</p>`,
                "en": `<p>With the growth of the company, I ended up taking this position to try to manage the technology sector.</p><p>Although it's a great challenge, we're adapting ourselves to obtain the best performance from all team members.</p><p>For example, instead of managing the scrum <small>(which was the initial idea)</small>, now I'm only helping on taking decisions. Most of the time, I keep developing other projects of the company.</p>`
            },
            "goals": [
                "goal_dynamic",
                "goal_sub_teams",
                "goal_two_week_sprint",
                "goal_long_backlog"
            ],
            "technologies": [
                "PHP",
                "JavaScript",
                "jQuery",
                "Laravel",
                "HTML5/SASS",
                "MySQL"
            ]
        },
        {
            "company": 1,
            "function": "developer",
            "from": 2018,
            "to": 2019,
            "resume": {
                "pt-br": `<p>Era, inicialmente, o único desenvolvedor da empresa. Passava a maior parte do tempo corrigindo problemas no projeto da empresa, pois já havia erros por diversas mudanças não-planejadas.</p>`,
                "en": `<p>I was the only developer in the company. I spent most of the time fixing problems in the company's project, as there were already errors due to several unplanned changes.</p>`
            },
            "goals": [],
            "technologies": [
                "PHP",
                "JavaScript",
                "jQuery",
                "Laravel",
                "HTML5/CSS3",
                "MySQL"
            ]
        },
        {
            "company": 0,
            "function": "developer",
            "from": 2014,
            "to": 2018,
            "resume": {
                "pt-br": `<p>Entrei como estagiário e depois fui efetivado. <b>60%</b> do meu conhecimento sobre desenvolvimento surgiu aqui.</p><p>Como era uma agência digital relativamente pequena, a maioria dos projetos no começo se resumiam a sites institucionais.</p><p>Porém, buscando novos desafios, passamos a desenvolver um gerenciador de projetos para projetos mais complexos.</p><p>Por exemplo, sites de escolas com painel do aluno, lojinhas on-line ou, até mesmo, a primeira versão <a href="https://explicae.com.br" target="_blank">projeto onde trabalhei em seguida</a>.</p>`,
                "en": `<p>I joined as an intern and then I was hired. <b>60%</b> of my development knowledge came here.</p><p>As it was a relatively small digital agency, most projects at the beginning were limited to institutional websites.</p><p>However, looking for new challenges, we started to develop a project manager for more complex projects.</p><p>For example, school websites with student panels, online stores or even the first project version <a href="https://explicae.com.br" target="_blank">where I worked next</a>.</p>`
            },
            "goals": [],
            "technologies": [
                "PHP",
                "JavaScript",
                "jQuery",
                "HTML5/CSS3",
                "MySQL"
            ]
        }
    ]
};

export default {
    namespaced: true,
    state
};
