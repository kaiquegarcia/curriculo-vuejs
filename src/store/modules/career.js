const state = {
    "pt-br": {
        "section": {
            "title": "Resumo de Carreira",
            "html" : `
        <p>
            Comecei a desenvolver oficialmente em <b>2014</b> após encerrar um curso prático de <b>PHP +
                Bootstrap</b>.
            Era estudante de <b>Ciência da Computação</b> na <b>Universidade Federal de Sergipe</b> e era,
            também, meu primeiro estágio.
        </p>
        <p>
            Eu era apenas <b>desenvolvedor back-end</b> com pouco conhecimento de front, mas, pela
            necessidade da agência onde trabalhei, aprendi a desenvolver os dois lados com alguns cursos que
            me disponibilizaram.<br/>
            Meu conhecimento está sempre evoluindo e tento manter a mente
            constantemente aberta para novos estudos.
        </p>
        <p class="mb-0">
            Hoje posso atuar como <b>desenvolvedor full stack</b> e continuo explorando a expansão dos meus domínios.
        </p>`
        }
    },
    "en": {
        "section": {
            "title": "Career Summary",
            "html" : `
        <p>
            I started to develop officially in <b>2014</b> after finishing a practical <b>PHP +
                Bootstrap</b> course. I was a <b>Computer Science</b> student at the <b>Federal University of
                Sergipe</b> and it was also my first internship.
        </p>
        <p>
            I was just a <b>back-end developer</b> with little knowledge of the front, but, due to the need of the
            agency where I worked, I learned to develop both sides with some courses that were made available to
            me.<br/>
            My knowledge is always evolving and I try to keep my mind constantly open to new studies.
        </p>
        <p class="mb-0">
            Today I can act as a <b>full stack developer</b> and I keep exploring the expansion of my domains.
        </p>`
        }
    }
};

export default {
    namespaced: true,
    state
};
