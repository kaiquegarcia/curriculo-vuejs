const state = {
    "i18n": {
        "pt-br": {
            "title": "Conhecimentos",
            "others": "Outros",
            "levels": [
                "junior",
                "intermed.",
                "senior",
                "especialista",
                "diretor"

            ]
        },
        "en": {
            "title": "Skills & Tools",
            "others": "Others",
            "levels": [
                "junior",
                "intermed.",
                "senior",
                "staff",
                "principal"
            ]
        }
    },
    "others": [
        "Code Review",
        "Git [Flow]",
        "Wireframing",
        "Scrum",
        "PSR",
        "Beanstalkd",
        "Drone.io",
        "Harness",
        "Sonar Cloud"
    ],
    "categories": [
        {
            "title": "Front-End",
            "list": [
                {
                    "name": "JavaScript",
                    "level": 2
                },
                {
                    "name": "HTML5/CSS3",
                    "level": 2
                },
                {
                    "name": "SASS/LESS",
                    "level": 1
                },
                {
                    "name": "Vue.js",
                    "level": 1
                },
                {
                    "name": "Node.js",
                    "level": 2
                },
                {
                    "name": "Angular",
                    "level": 1
                },
                {
                    "name": "React",
                    "level": 1
                }
            ]
        },
        {
            "title": "Back-End",
            "list": [
                {
                    "name": "PostgreSQL",
                    "level": 2
                },
                {
                    "name": "Golang",
                    "level": 3
                },
                {
                    "name": "MySQL",
                    "level": 2
                },
                {
                    "name": "PHP",
                    "level": 3
                },
                {
                    "name": "Laravel",
                    "level": 3
                },
                {
                    "name": "Kafka",
                    "level": 2
                },
                {
                    "name": "Node.js",
                    "level": 3
                },
                {
                    "name": "MongoDB",
                    "level": 2
                },
                {
                    "name": "Redis",
                    "level": 2
                }
            ]
        }
    ]
};

export default {
    namespaced: true,
    state
};
