const state = {
    "lang": "pt-br",
    "i18n": {
        "pt-br": {
            "ask_pwa": "Você quer adicionar meu site ao seu sistema operacional?",
            "pwa_yes": "Sim",
            "pwa_no": "Deixa pra outro dia"
        },
        "en": {
            "ask_pwa": "Do you want to add my website to your operational system?",
            "pwa_yes": "Yeah",
            "pwa_no": "Maybe later"
        }
    }
};

const mutations = {
    lang: (state, lang) => state.lang = lang
};

const actions = {
    setLang({commit}, lang) {
        let acceptable = ["pt-br", "en"];
        if (acceptable.indexOf(lang) === -1) {
            throw "Invalid language";
        }
        commit("lang", lang);
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
};