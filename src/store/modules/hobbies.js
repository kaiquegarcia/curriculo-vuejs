const state = {
    "pt-br": {
        "section": {
            "title": "Hobbies"
        },
        "list": [
            {
                "link": "",
                "label": "Investir"
            },
            {
                "link": "https://soundcloud.com/kik0w",
                "label": "Música"
            },
            {
                "link": "",
                "label": "Escrever"
            },
            {
                "link": "",
                "label": "Desenhar"
            },
            {
                "link": "",
                "label": "Incentivar ideias"
            },
            {
                "link": "",
                "label": "Empreender"
            },
        ]
    },
    "en": {
        "section": {
            "title": "Hobbies"
        },
        "list": [
            {
                "link": "",
                "label": "Invest"
            },
            {
                "link": "https://soundcloud.com/kik0w",
                "label": "Play Music"
            },
            {
                "link": "",
                "label": "Write"
            },
            {
                "link": "",
                "label": "Draw"
            },
            {
                "link": "",
                "label": "Encourage ideas"
            },
            {
                "link": "",
                "label": "Launch business"
            },
        ]
    }
};

export default {
    namespaced: true,
    state
};