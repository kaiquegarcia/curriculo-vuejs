const state = {
    "pt-br": {
        "section": {
            "title": "Línguas"
        },
        "list": [
            {
                "name": "Português",
                "level": "nativo"
            },
            {
                "name": "Inglês",
                "level": "intermediário"
            },
            {
                "name": "Espanhol",
                "level": "iniciante"
            }
        ]
    },
    "en": {
        "section": {
            "title": "Languages"
        },
        "list": [
            {
                "name": "Brazilian Portuguese",
                "level": "native"
            },
            {
                "name": "English",
                "level": "intermediate"
            },
            {
                "name": "Spanish",
                "level": "beginner"
            }
        ]
    }
};

export default {
    namespaced: true,
    state
};