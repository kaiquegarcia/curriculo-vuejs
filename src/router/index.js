import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import Home from "./../components/home";
import NotFoundPage from "./../components/404";

const routes = [
    {
        path: "/en",
        redirect: "/lang/en"
    },
    {
        path: "/lang/:language",
        component: Home
    },
    {
        path: "/",
        component: Home
    },
    {
        path: "*",
        component: NotFoundPage,
    }
];

export default new VueRouter({
    mode: "history",
    routes
})