module.exports = {
    pwa: {
        name: "Kaique Garcia",
        themeColor: "#22A162",
        msTileColor: "#F7F8FA",
        assetsVersion: "1.0.3",
        icons: [
            {
                src: 'img/icons/android-chrome-192x192.png',
                sizes: '192x192',
                type: 'image/png'
            },
            {
                src: 'img/icons/android-chrome-512x512.png',
                sizes: '512x512',
                type: 'image/png'
            }
        ],
        iconPaths: {
            favicon16: 'img/icons/favicon-16x16.png',
            favicon32: 'img/icons/favicon-32x32.png',
            appleTouchIcon: 'img/icons/apple-touch-icon.png'
        },
        manifestOptions: {
            name: "Kaique Garcia",
            short_name: "Kaique",
            start_url: ".",
            display: "standalone",
            theme_color: "#22A162",
            background_color: "#F7F8FA"
        }
    }
}