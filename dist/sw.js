importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.0.1/workbox-sw.js');

workbox.setConfig({ debug: false });
workbox.precaching.precacheAndRoute([]);

// Cache images:
workbox.routing.registerRoute(
    /\.(?:png|gif|jpg|jpeg|svg|ico)$/,
    workbox.strategies.staleWhileRevalidate({
        cacheName: "images",
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 60,
                maxAgeSeconds: 30 * 24 * 60 * 60 // 30 days
            })
        ]
    })
);

// Cache Google fonts:
workbox.routing.registerRoute(
    new RegExp("https://fonts.(?:googleapis|gstatic).com/(.*)"),
    workbox.strategies.cacheFirst({
        cacheName: "googleapis",
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 30,
                maxAgeSeconds: 30 * 24 * 60 * 60 // 30 days
            })
        ]
    })
);

// Cache Font Awesome:
workbox.routing.registerRoute(
    new RegExp("https://use.fontawesome.com/(.*)"),
    workbox.strategies.cacheFirst({
        cacheName: "fontawesome",
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 30,
                maxAgeSeconds: 30 * 24 * 60 * 60 // 30 days
            })
        ]
    })
);